$(document).ready(function () {

    $(".titleDropdown").select2({
        placeholder: "Select a title",
        minimumResultsForSearch: -1,
        theme: 'reg-form'
    });

    let firstRadioFormSelectedElement;
    let secondRadioFormSelectedElement;
    let thirdRadioFormSelectedElement;

    let $firstName = $('#firstName');
    let $lastName = $('#lastName');
    let $email = $('#email');
    let $password = $('#password');
    let $repPassword = $('#repPassword');
    let $yearsCheckBox = $('.18years');
    let $statementsCheckBox = $('.statements');
    let $registerButton = $('.registerButton');
    let $titleDropdown = $('.titleDropdown');


    let isError = {
        firstName: false,
        lastName: false,
        email: false,
        password: false
    }

    let $customRadioButtons = $(".customRadioButton");

    $customRadioButtons.click(customRadioHandler);
    $customRadioButtons.click(validateInputs);

    let $inputs = $("input");

    $inputs.keyup(inlineValidationHandler);
    $inputs.keyup(validateInputs);

    let $inputChecks = $(".inputCheckContainer");

    $inputChecks.click(checkedHandler);
    $inputChecks.click(validateInputs);

    let $dropdownMenu = $('.titleDropdown');

    $dropdownMenu.change(validateInputs);

    $registerButton.click(displayResults);

    function checkedHandler(e) {

        let targetElement = e.target;

        $(targetElement).hasClass('input-checked')
            ?
            $(targetElement).removeClass('input-checked')
            :
            $(targetElement).addClass('input-checked');
    }

    function customRadioHandler(e) {
        let elementForm = $(e.target).prop('classList')[1];
        if (elementForm === 'firstForm') {

            if (!firstRadioFormSelectedElement) {
                firstRadioFormSelectedElement = e.target;
            }
            else {
                if (firstRadioFormSelectedElement !== e.target && firstRadioFormSelectedElement !== $(e.target).parent()) {
                    $(firstRadioFormSelectedElement).removeClass('checked');

                    const oval2Picture = $(firstRadioFormSelectedElement).find('.oval2')[0];
                    oval2Picture.style.display = 'none';

                    firstRadioFormSelectedElement = e.target;
                }
            }
            $(firstRadioFormSelectedElement).addClass('checked');

            const oval2Picture = $(firstRadioFormSelectedElement).find('.oval2')[0];

            oval2Picture.style.display = 'block';
            oval2Picture.style.margin = 'auto';
            oval2Picture.style.marginTop = '3px';

        }
        if (elementForm === 'secondForm') {
            if (!secondRadioFormSelectedElement) {
                secondRadioFormSelectedElement = e.target;
            }
            else {
                if (secondRadioFormSelectedElement !== e.target && secondRadioFormSelectedElement !== $(e.target).parent()) {
                    $(secondRadioFormSelectedElement).removeClass('checked');

                    const oval2Picture = $(secondRadioFormSelectedElement).find('.oval2')[0];
                    oval2Picture.style.display = 'none';

                    secondRadioFormSelectedElement = e.target;
                }
            }

            $(secondRadioFormSelectedElement).addClass('checked');

            const oval2Picture = $(secondRadioFormSelectedElement).find('.oval2')[0];

            oval2Picture.style.display = 'block';
            oval2Picture.style.margin = 'auto';
            oval2Picture.style.marginTop = '3px';
        }
        if (elementForm === 'thirdForm') {
            if (!thirdRadioFormSelectedElement) {
                thirdRadioFormSelectedElement = e.target;
            }
            else {
                if (thirdRadioFormSelectedElement !== e.target && thirdRadioFormSelectedElement !== $(e.target).parent()) {
                    $(thirdRadioFormSelectedElement).removeClass('checked');

                    const oval2Picture = $(thirdRadioFormSelectedElement).find('.oval2')[0];
                    oval2Picture.style.display = 'none';

                    thirdRadioFormSelectedElement = e.target;
                }
            }

            $(thirdRadioFormSelectedElement).addClass('checked');

            const oval2Picture = $(thirdRadioFormSelectedElement).find('.oval2')[0];

            oval2Picture.style.display = 'block';
            oval2Picture.style.margin = 'auto';
            oval2Picture.style.marginTop = '3px';
        }
    }

    function inlineValidationHandler(e) {
        if (e.target.id !== 'titleDropdown') {
            const element = e.target;
            const $elementLabel = $(element).prev().find('label');
            let errorMessage = $elementLabel.next()[0];

            if (e.target.id === 'firstName' || e.target.id === 'lastName') {

                const letterRegex = /[^a-zA-Z]/;

                if (element.value.match(letterRegex)) {

                    e.target.id === 'firstName' ? isError.firstName = true : isError.lastName = true;

                    $elementLabel.addClass('label-error');
                    errorMessage.textContent = `Please enter a valid ${$elementLabel[0].textContent.toLocaleLowerCase()}`;

                    errorMessage.style.display = 'block';

                    $(element).addClass('error');
                }
                else {

                    $elementLabel.removeClass('label-error');

                    errorMessage.style.display = 'none';

                    $(element).removeClass('error');

                    e.target.id === 'firstName' ? isError.firstName = false : isError.lastName = false;
                }
            }
            if (e.target.id === 'email') {

                if (validateEmail(e.target.value) === false) {

                    isError.email = true;
                    $elementLabel.addClass('label-error');
                    errorMessage.textContent = `Please enter a valid ${$elementLabel[0].textContent.toLocaleLowerCase()}`;
                    errorMessage.style.display = 'block';

                    $(element).addClass('error');
                }
                else {
                    isError.email = false;
                    $elementLabel.removeClass('label-error');

                    errorMessage.style.display = 'none';

                    $(element).removeClass('error');
                }
            }
            if (e.target.id === 'repPassword') {
                let passwordValue = $('#password').value;
                let repeatPasswordValue = $('#repPassword').value;

                if (passwordValue !== repeatPasswordValue) {
                    isError.password = true;
                    $elementLabel.addClass('label-error');

                    errorMessage.textContent = `Please enter matching passwords`;
                    errorMessage.style.display = 'block';

                    $(element).addClass('error');
                }
                else {
                    isError.password = false;
                    $elementLabel.removeClass('label-error');

                    errorMessage.style.display = 'none';

                    $(element).removeClass('error');
                }
            }
        }

    }


    function validateEmail(emailAdress) {
        let regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (emailAdress.match(regexEmail)) {
            return true;
        } else {
            return false;
        }
    }

    function validateInputs() {


        if ($firstName.val() !== '' && $lastName.val() !== '' && $email.val() !== '' && $password.val() !== '' && $repPassword.val() !== ''
            && $titleDropdown.find(':selected')[0].textContent !== '' && $yearsCheckBox.hasClass('input-checked') && $statementsCheckBox.hasClass('input-checked') &&
            firstRadioFormSelectedElement !== undefined && secondRadioFormSelectedElement !== undefined && thirdRadioFormSelectedElement !== undefined &&
            isError.firstName === false && isError.lastName === false && isError.email === false && isError.password === false) {

            $registerButton.removeClass('disabled');
        }
        else {
            $registerButton.addClass('disabled');
        }

    }
    
    function displayResults(e){

        e.preventDefault();

        let result = {
            firstName: $firstName.val(),
            lastName: $lastName.val(),
            email: $email.val(),
            password: $password.val(),
            title: $titleDropdown.find(':selected').text(),
            firstQuestion: $(firstRadioFormSelectedElement).parent().find('label').text(),
            secondQuestion: $(secondRadioFormSelectedElement).parent().find('label').text(),
            thirdQuestion: $(thirdRadioFormSelectedElement).parent().find('label').text(),
        }

        console.log(result);
    }
})


